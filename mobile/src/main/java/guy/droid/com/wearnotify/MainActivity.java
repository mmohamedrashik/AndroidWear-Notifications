package guy.droid.com.wearnotify;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.support.v4.app.NotificationCompat.WearableExtender;

public class MainActivity extends AppCompatActivity {
    Button button;
    public  static final String EXTRA_VOICE_REPLY  = "extra_reply";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button)findViewById(R.id.not_one);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /** REMOTE VOICE INPUT FROM WEAR **/
                RemoteInput remoteInput = new RemoteInput.Builder(EXTRA_VOICE_REPLY)
                        .setLabel("VOICE HERE").build();

                Intent actionIntent = new Intent(getApplicationContext(), NotifyScreen.class);

                PendingIntent actionPendingIntent =
                        PendingIntent.getActivity(getApplicationContext(), 0, actionIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT);


                NotificationCompat.Action action =
                        new NotificationCompat.Action.Builder(R.drawable.notification,
                                "HELLO", actionPendingIntent)
                                .build(); // Action for Notification in Status Bar
                NotificationCompat.Action action2 =
                        new NotificationCompat.Action.Builder(R.drawable.notification,
                                "VOICE HERE", actionPendingIntent).addRemoteInput(remoteInput)
                                .build(); // Action for Notification in Status Bar

                Bitmap background  = BitmapFactory.decodeResource(getResources(),R.drawable.mcd);
                // SECOND PAGE
                NotificationCompat.BigTextStyle secondpageStyle = new NotificationCompat.BigTextStyle();
                secondpageStyle.bigText("RAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

                Notification secondpages = new NotificationCompat.Builder(getApplicationContext())
                        .setContentTitle("SECOND PAGE")
                        .setStyle(secondpageStyle)
                        .build();

                NotificationCompat.WearableExtender wearableExtender = new NotificationCompat.WearableExtender().addAction(action).addAction(action2).setBackground(background).addPage(secondpages); /** Wearable Extender: These actions are only for watches **/
                NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                        .setContentTitle("Notification").setColor(Color.BLACK)
                        .setContentText("New Notification Received").setAutoCancel(true).setContentIntent(actionPendingIntent) /** Set Action For Comlete Notification Click **/
                        .setSmallIcon(R.drawable.notification).extend(wearableExtender).addAction(action); /** These actions are for mobiles **/

                int notificationId = 2; /** Notification ID - Same ID Should be used to clear notification :: check NotifyScreen.java **/

                NotificationManagerCompat mgr = NotificationManagerCompat.from(getApplicationContext());
                mgr.notify(notificationId,builder.build()); /**  Build Notification **/


            }
        });
    }
}
