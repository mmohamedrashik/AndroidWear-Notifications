package guy.droid.com.wearnotify;

import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class NotifyScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_screen);
        int notificationId = 2;

        String voice_data = "";

        NotificationManagerCompat mgr = NotificationManagerCompat.from(getApplicationContext());
        mgr.cancel(notificationId); // Cancel The Current Notification
        try
        {
            Bundle remoteInput = RemoteInput.getResultsFromIntent(getIntent());
            if(remoteInput!=null)
            {
                voice_data = remoteInput.getCharSequence(MainActivity.EXTRA_VOICE_REPLY).toString();
                Toast.makeText(getApplicationContext(),voice_data,Toast.LENGTH_LONG).show();
            }
        }catch (Exception e)
        {

        }
    }
}
